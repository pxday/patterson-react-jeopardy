import React from "react";
import AnswerForm from "./AnswerForm";

function GameBoard(props) {
  const category = props.data.category && props.data.category.title;

  return (
    <div className="GameBoard">
      <h2>Category: {category}</h2>
      <h3>{props.data.value}</h3>
      <div className="clue">{props.data.question}</div>
      <AnswerForm checkAnswer={props.checkAnswer} />
      <div className="score">Winnings: ${props.score}</div>
    </div>
  );
}

export default GameBoard;
